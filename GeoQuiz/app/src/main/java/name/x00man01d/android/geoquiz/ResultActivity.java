package name.x00man01d.android.geoquiz;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    private static final String EXTRA_SCORE =
            "com.bignerdranch.android.geoquiz.score";

    private TextView resultView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        resultView = findViewById(R.id.result_text_view);
        int score = getIntent().getIntExtra(EXTRA_SCORE, 0);
        resultView.setText(String.valueOf(score));
    }

    public static Intent newIntent(Context packageContext, int score) {
        Intent intent = new Intent(packageContext, ResultActivity.class);
        intent.putExtra(EXTRA_SCORE, score);
        return intent;
    }
}
